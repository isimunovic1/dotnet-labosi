﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using InternetShop.Models;

namespace InternetShop.Data
{
    public class InternetShopContext : DbContext
    {
        public InternetShopContext (DbContextOptions<InternetShopContext> options)
            : base(options)
        {
        }

        public DbSet<InternetShop.Models.Item> Item { get; set; }

        public DbSet<InternetShop.Models.Category> Category { get; set; }
    }
}
