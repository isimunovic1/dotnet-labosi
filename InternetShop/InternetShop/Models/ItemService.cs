﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetShop.Data;
using Microsoft.EntityFrameworkCore;
namespace InternetShop.Models
{
    public class ItemService : IItemService
    {
        public ItemService(InternetShopContext db)
        {
            this.db = db;
        }

        public IEnumerable<Item> AllItems()
        {
            return (from i in db.Item
                   select i).Include("Category");
        }
        public IEnumerable<Item> SortAmountHighToLow()
        {
            return (from i in db.Item
                   where i.AmountAvailable > 0 == true
                   orderby i.AmountAvailable descending
                   select i).Include("Category");
        }

        

        public IEnumerable<Item> SortPriceHighToLow()
        {
            return (from i in db.Item  
                   orderby i.Price ascending
                   select i).Include("Category");
        }
        
        public IEnumerable<Item> OnDiscount()
        {
            return (from i in db.Item
                   where i.Action == true
                   select i).Include("Category");
        }
        /*
        public int calculateDiscount(Item item)
        {
            Discount = ((item.ActionPrice - item.Price) / item.Price) * 100;
            return Discount;
        }*/

        //public int Discount { get; set; }
        public InternetShopContext Db { get => db; }
        private readonly InternetShopContext db;
    }
}
