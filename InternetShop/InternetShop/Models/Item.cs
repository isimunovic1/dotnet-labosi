﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InternetShop.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }

        public int AmountAvailable { get; set; }

        public bool Action { get; set; }

        public decimal ActionPrice { get; set; }

        public string SalePercentage
        {
            get
            {
                return (@Math.Round(((Price - ActionPrice) / Price) * 100, 0)).ToString();
            }
        }

        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
