﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternetShop.Data;

namespace InternetShop.Models
{
    public interface IItemService
    {
         IEnumerable<Item> AllItems();
         IEnumerable<Item>  SortAmountHighToLow();
         IEnumerable<Item> SortPriceHighToLow();
        IEnumerable<Item> OnDiscount();

        //int calculateDiscount(Item);
        InternetShopContext Db { get; }
        //int Discount { get; set; }
    }
}
