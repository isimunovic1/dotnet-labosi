﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InternetShop.Data;
using InternetShop.Models;

namespace InternetShop
{
    public class ItemsController : Controller
    {
        
        private readonly IItemService itemService;
        public ItemsController(IItemService itemService)
        {
            this.itemService = itemService;
        }

        // GET: Items
        public IActionResult Index()
        {
            return View(itemService.AllItems());
        }

        // GET: Items/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = itemService.Db.Item
                .FirstOrDefault(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            ViewData["Categories"] = new SelectList(itemService.Db.Category, "CategoryId", "Name");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("Id,Name,Description,Price,AmountAvailable,Action,ActionPrice,CategoryId")] Item item)
        {
            if (ModelState.IsValid)
            {
                itemService.Db.Add(item);
                itemService.Db.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Categories"] = new SelectList(itemService.Db.Category, "CategoryId", "Name");
            return View(item);
        }

        // GET: Items/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var item = itemService.Db.Item.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            ViewData["Categories"] = new SelectList(itemService.Db.Category, "CategoryId", "Name");
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,Name,Description,Price,AmountAvailable,Action,ActionPrice,CategoryId,Category")] Item item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    itemService.Db.Update(item);
                    itemService.Db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        // GET: Items/Delete/5
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = itemService.Db.Item
                .FirstOrDefault(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var item = itemService.Db.Item.Find(id);
            itemService.Db.Remove(item);
            itemService.Db.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemExists(int id)
        {
            return itemService.Db.Item.Any(e => e.Id == id);
        }
        
        public IActionResult SortPrice()
        {
            return View(itemService.SortPriceHighToLow());
        }

        public IActionResult SortAmount()
        {
            return View(itemService.SortAmountHighToLow());
        }

        public IActionResult OnDiscount()
        {
            return View(itemService.OnDiscount());
        }
        /*
        public int Discount(Item item)
        {
            return itemService.calculateDiscount(item);
        }*/
    }
}
