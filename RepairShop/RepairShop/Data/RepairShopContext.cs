﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RepairShop.Models;

namespace RepairShop.Data
{
    public class RepairShopContext : DbContext
    {
        public RepairShopContext (DbContextOptions<RepairShopContext> options)
            : base(options)
        {
        }

        public DbSet<RepairShop.Models.Ticket> Ticket { get; set; }
    }
}
