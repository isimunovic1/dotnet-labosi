﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RepairShop.Data;
using RepairShop.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RepairShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly RepairShopContext _context;

        public int counter;
        public int compCounter;
        public int uncomCounter;

        public HomeController(ILogger<HomeController> logger, RepairShopContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<IActionResult> IndexAsync()
        {
            foreach(var ticket in _context.Ticket)
            {
                if (ticket.Completed == true)
                {
                    compCounter++;
                }
                else
                {
                    uncomCounter++;
                }
                counter++;
            }
            ViewBag.compCounter = compCounter;
            ViewBag.uncomCounter = uncomCounter;
            ViewBag.counter = counter;
            return View(await _context.Ticket.ToListAsync());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
    }
}
